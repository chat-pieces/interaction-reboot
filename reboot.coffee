fs = require 'fs'

# Só faz sentido sendo iniciado de alguma forma como essa:
# npm run daemon

dir = __dirname
pack = version: 'not-found', dependencies: { 'small-simple-bot': '0.0.0' }
packLock = dependencies : { 'small-simple-bot': '0.0.0' }
while dir and pack.name isnt process.env.npm_package_name
    dir = dir.replace /\/[^\/]+\/*$/, ''
    try
        pack = require dir + '/package.json'
        packLock = require dir + '/package-lock.json'

dependencies = Object.keys(pack.dependencies)
    .map (key)->
        key + ': ' + packLock.dependencies[key].version
    .join '\n'

chatFile = (bot)-> "/tmp/bot-#{bot.token}-killer-chat"

module.exports = reboot = (bot, update)->
    msg = update.message?.text or ''
    usr = update.message?.from.username or ''
    chat = update.message?.chat.id or 0

    return unless bot.isAdm usr

    if msg.match /^\/reboot|^\s*morra\s*$/i
        fs.writeFile chatFile(bot), chat.toString(), (err)->
            if err
                bot.logger.error err
                bot.sendMessage "Não posso morrer...\n#{err.message}", chat
            else
                bot.sendMessage 'Morri', chat, {}, ->
                    bot.stop true

module.exports.init = (bot)->
    msg = """
    Voltei!
    #{pack.name}: #{pack.version}
    #{dependencies}
    """
    fs.readFile chatFile(bot), 'utf8', (err, chat)->
        if err?.code is 'ENOENT'
            return bot.logger.log "#{chatFile bot} não existe. Primeira vez?"
            bot.msgToAdms msg
        else
            return bot.logger.error err if err
            bot.logger.log "Notificar retorno no chat #{chat}"
            bot.sendMessage msg, chat

